'''
该模块包含一些常用的函数
'''

from tkinter import *
from PIL import Image,ImageTk

def set_center(window, w, h):  
    """设置窗口大小并居中"""
    ws = window.winfo_screenwidth()
    hs = window.winfo_screenheight()
    x = (ws / 2) - (w / 2)
    y = (hs / 2) - (h / 2)
    window.geometry("{:.0f}x{:.0f}+{:.0f}+{:.0f}".format(w, h, x, y))

def get_image(filename,width,height):
    """获取图片并缩放大小"""
    im=Image.open(filename).resize((width,height))
    return ImageTk.PhotoImage(im)
