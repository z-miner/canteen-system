'''
主函数，程序入口
'''
from tkinter import *
import tkinter
from customFrame import *
from tkinter import ttk
import customTool as tool


def main():
    """主函数"""
    #生成登录窗口
    login_win=LoginWindow()
    #开启主循环
    login_win.mainloop()
    
main()